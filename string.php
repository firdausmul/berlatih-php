<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h2>Contoh Soal</h2>
    <?php

        echo "<h3>Soal No 1</h3>";
        $kalimat1 = "Hello PHP!";
        echo "kalimat pertama : " . $kalimat1 . "<br>";
        echo "panjang string :" . strlen($kalimat1) . "<br>";
        echo "jumlah kata :" . str_word_count($kalimat1) . "<br><br>";


        echo "<h3>Soal No 2</h3>";
        $string2 = "I Love PHP";
        echo "kalimat kedua : " .  $string2 . "<br>";
        echo "kata pertama : " . substr($string2,0,1) . "<br>";
        echo "kata kedua : " . substr($string2,2,4) . "<br>";
        echo "kata ketiga : " . substr($string2,7,3). "<br>";


        echo "<h3>Soal No 3</h3>";
        $string3 = "PHP is old but good";
        echo "kalimat ketiga : " . $string3 . "<br>";
        echo "ganti kata kelima :" . str_replace("good", "awesome",$string3);
    ?>
</body>
</html>