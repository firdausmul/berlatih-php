<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Array</title>
</head>
<body>
    <h1>Berlatih Array</h1>
    <?php
        echo "<h2>Soal 1</h2>" . "<br>";
        $Kids = ["Mike", "Dustin", "Will", "Lucas", "Max", "Eleven"];
        print_r($Kids);
        echo "<br>";
        $Adults = ["Hopper", "Nancy",  "Joyce", "Jonathan", "Murray"];
        print_r($Adults);

        echo "<h2>Soal 2</h2>";
        echo "Total Kids ". count($Kids);
        echo "<ol>";
        echo "<li>" . $Kids[0] . "</li>";
        echo "<li>" . $Kids[1] . "</li>";
        echo "<li>" . $Kids[2] . "</li>";
        echo "<li>" . $Kids[3] . "</li>";
        echo "<li>" . $Kids[4] . "</li>";
        echo "<li>" . $Kids[5] . "</li>";
        echo "</ol>";
        echo "<br>";
        echo "Total Adults " . count($Adults);
        echo "<ol>";
        echo "<li>" . $Adults[0] . "</li>";
        echo "<li>" . $Adults[1] . "</li>";
        echo "<li>" . $Adults[2] . "</li>";
        echo "<li>" . $Adults[3] . "</li>";
        echo "<li>" . $Adults[4] . "</li>";
        echo "</ol>";

        echo "<h2>Soal 3</h2>";
        $Biodata = [
            ["Name" => "Will Byers", "Age" => "12", "Aliases" => "Will The Wise", "Status" => "Alive"],
            ["Name" => "Mike Wheeler", "Age" => "12", "Aliases" => "Dugeon Master", "Status" => "Alive"],
            ["Name" => "Jim Hooper", "Age" => "43", "Aliases" => "Chief Hopper", "Status" => "Deceased"],
            ["Name" => "Eleven", "Age" => "12", "Aliases" => "E1", "Status" => "Alive"],
        ];

        echo "<pre>";
        print_r($Biodata);
        echo "</pre>";
    ?>
</body>
</html>